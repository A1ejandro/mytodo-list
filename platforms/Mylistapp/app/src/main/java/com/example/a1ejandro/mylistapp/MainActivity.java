package com.example.a1ejandro.mylistapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] tvShow = {"Tekkaman Blade", "Samurai Champloo", "Cowboy Bebop", "Gurren Laggan Tengen Toppen"};

        ListAdapter theAdapter = new myAdapter(this, tvShow);

        ListView listView = (ListView)findViewById(R.id.theListView);

        //tell listview how to adapt data to it, which also includes the data
        listView.setAdapter(theAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String tvShowPicked = String.valueOf(adapterView.getItemAtPosition(i));

                Toast.makeText(MainActivity.this, "You selected " + tvShowPicked + "!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
