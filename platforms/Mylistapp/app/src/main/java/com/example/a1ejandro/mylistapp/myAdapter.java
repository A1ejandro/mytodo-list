package com.example.a1ejandro.mylistapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

//Adapter helps take some data and try adapt it into some sort of layout view
public class myAdapter extends ArrayAdapter<String> {

    public myAdapter(Context context, String[] values)
    {
        super(context, R.layout.rowlayout, R.id.rowText, values);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //return super.getView(position, convertView, parent);


        //Layout inflates are good for building layouts from xml files and populating them with data
        LayoutInflater layoutInflater =  LayoutInflater.from(getContext());

        //Inflate takes a resource to load, the parent it may be loaded into
        //and true/false ir we are loading it into a parent view
        View view = layoutInflater.inflate(R.layout.rowlayout, parent, false);

        String tvShow = getItem(position);

        //Find the text view rowText within out layout
        TextView rowText = (TextView) view.findViewById(R.id.rowText);

        rowText.setText(tvShow);

        //Every second item in the list
        if(position%2 == 0)
        {
            ImageView rowImage = (ImageView) view.findViewById(R.id.rowImage);
            rowImage.setImageResource(R.drawable.flower);

        }

        return view;
    }
}
