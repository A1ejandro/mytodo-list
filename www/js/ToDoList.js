window.onload = init;

function init(){
  var button = document.getElementById('addBtn');
  button.onclick = createListItem;

  var clearButton = document.getElementById('clearAll');
  clearButton.onclick = clearStorage;


  for (var i = 0; i < localStorage.length; i++) {
    var key = localStorage.key(i);
    if (key.substring(0, 6) == 'sticky') {
      var value = localStorage.getItem(key);
      addItemTodo(value);
    }
  }

}

function clearStorage(){
  localStorage.clear();
  location.reload();
}

function createListItem(){
  var value = document.getElementById('userInput').value;
  var key = 'sticky_' + localStorage.length;
  localStorage.setItem(key, value);
  addItemTodo(value);

}

function emptyInput(){
  var value = document.getElementById('userInput').value;
  if (value){
    addItemTodo(value);
    document.getElementById('userInput').value = '';
  }
}
/*
document.getElementById('addBtn').addEventListener('touchend', function(){
  var value = document.getElementById('userInput').value;
  if (value){
    addItemTodo(value);
    document.getElementById('userInput').value = '';
  }

});*/

// remove and complete icons
var removeBtn = '<a href="#"><span class="glyphicon glyphicon-remove"></span></a>';
var completeBtn = '<a href="#"><span class="glyphicon glyphicon-ok"></span></a>';


// User click on the add button
//If there is any text inside the item field, add that text to the todo list


function removeItem(){
  var item = this.parentNode.parentNode;
  var parent = item.parentNode;

  //console.log(item);
  parent.removeChild(item);

}

function completedItem(){
  var item = this.parentNode.parentNode;
  var parent = item.parentNode;
  var id = parent.id;

  var target;

  if (id == 'todo'){
    // it's a todo item to be completed
    target = document.getElementById('completed');
  }else {
    //it's a complete item to be re-done
    target = document.getElementById('todo');
  }
  parent.removeChild(item);
  target.insertBefore(item, target.childNodes[0]);
}

// Adds a new item to the todo list
function addItemTodo(value){

 var list = document.getElementById('todo');

 var item = document.createElement('li');
 item.innerText = value;

 var buttons = document.createElement('div');
 buttons.classList.add('buttons');

 var remove = document.createElement('button');
 remove.classList.add('remove');
 remove.innerHTML = removeBtn;

 // Add click event for removing item
 remove.addEventListener('touchend', removeItem);

 var complete = document.createElement('button');
 complete.classList.add('complete');
 complete.innerHTML = completeBtn;

 // Add click events for completed items
 complete.addEventListener('touchend', completedItem);


 buttons.appendChild(remove);
 buttons.appendChild(complete);
 item.appendChild(buttons);
 list.appendChild(item);

 list.insertBefore(item, list.childNodes[0]);

}
